<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');
require_once 'Twig/Autoloader.php';
/**
* 
*/
class Twig
{
    public $twig_env;

    private $_template_dir;
    private $_cache_dir;
    private $_tpl_sub_dir;

    public function __construct()
    {
        $this->CI =& get_instance();
        /*
        ini_set('include_path',
        ini_get('include_path') . PATH_SEPARATOR . APPPATH . 'libraries/Twig');
        require_once (string) "Autoloader" . EXT;
        */
        Twig_Autoloader::register();
        log_message('debug', 'twig autoloader loaded');
        $this->cache_flag = FALSE;
        $this->_templates_dir = APPPATH . 'templates';
        $this->_cache_dir = APPPATH . 'cache';
        $debug = TRUE;
        $loader = new Twig_Loader_Filesystem($this->_templates_dir);
        $this->twig_env = new Twig_Environment($loader, array(
                //'cache' => $this->_cache_dir,
                'debug' => $debug,
        ));

    }

    public function setTemplatesSubDir($in_dir)
    {
        $this->_tpl_sub_dir = $in_dir;
    }

    public function render($template, $data = array())
    {
        $template = $this->twig_env->loadTemplate($template);
        return $template->render($data);
    }


    public function displayStats($template, $data = array())
    {
        $template = $this->twig_env->loadTemplate($template);
        /* elapsed_time and memory_usage */
        $data['elapsed_time'] = $this->CI->benchmark->elapsed_time('total_execution_time_start', 'total_execution_time_end');
        $memory = (!function_exists('memory_get_usage')) ? '0' : round(memory_get_usage()/1024/1024, 2);
        $data['memory_usage'] = $memory;
        $template->display($data);
    }

    public function display($template, $data = array()) {
        $templateObj = $this->twig_env->loadTemplate($template);
        $data['elapsed_time'] = $this->CI->benchmark->elapsed_time('total_execution_time_start', 'total_execution_time_end');
        $memory = (!function_exists('memory_get_usage')) ? '0' : round(memory_get_usage()/1024/1024, 2);
        $data['memory_usage'] = $memory;
        $this->CI->output->set_output($templateObj->render($data));
    }
   
}